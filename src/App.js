import React from "react";
import Products from "./components/Products";
import Navbar from "./components/Navbar";
import Men from "./components/Men";
import Category from "./components/Category";
import Women from "./components/Women";
import Electronic from "./components/Electronic";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Jewellary from "./components/Jewellary";
export default class App extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        <Navbar />
        <Category />
        <Switch>
          <Route exact path="/" component={Products} />
          <Route exact path="/men" component={Men} />
          <Route exact path="/women" component={Women} />
          <Route exact path="/jewelery" component={Jewellary} />
          <Route exact path="/electronics" component={Electronic} />
        </Switch>
      </Router>
    );
  }
}
