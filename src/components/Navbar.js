import React from "react";
import { Link } from "react-router-dom";
export default class Navbar extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <header className="header">
        <nav className="nav-items">
          <div className="logo-box">
            <Link to="/" className="logo">
              Show<b style={{ color: "red" }}>N</b>ow
            </Link>
          </div>
          <div className="search-box">
            <input
              type={`text`}
              placeholder="Search for brands, products and more"
            />
            <span className="search-icon">
              <Link className="icon-link">
                <i class="fas fa-search"></i>
              </Link>
            </span>
          </div>
          <ul>
            <li>
              <Link className="login-link">Login</Link>
            </li>
            <li>
              <Link className="cart-link">
                <i class="fas fa-shopping-cart"><b style={{padding:"2px"}}>Cart</b></i>
              </Link>
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}
