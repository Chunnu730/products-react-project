import React from "react";
import { Link } from "react-router-dom";

export default class Category extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="container text-center mt-2">
        <div class="dropdown show">
          <Link
            class="btn  dropdown-toggle"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Category
          </Link>

          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <Link to="/" class="dropdown-item category-link">
              All
            </Link>
            <Link to="/men" class="dropdown-item category-link">
              Men's clothing
            </Link>
            <Link to="/women" class="dropdown-item category-link">
              Women's clothing
            </Link>
            <Link to="/jewelery" class="dropdown-item category-link">
              Jewellary
            </Link>
            <Link to="/electronics" class="dropdown-item category-link">
              Electronics
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
