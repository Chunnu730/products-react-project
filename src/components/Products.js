import React from "react";
export default class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productsData: [],
      isDataLoaded: false,
    };
  }

  async componentDidMount() {
    const products = await fetch("https://fakestoreapi.com/products");
    const json = await products.json();
    this.setState({
      productsData: json,
      isDataLoaded: true,
    });
  }
  render() {
    const { productsData, isDataLoaded } = this.state;
    if (!isDataLoaded) {
      return <div className="loader"></div>;
    }
    return (
      <>
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 text-center text-secondary p-4">
              <p>Top fashion items</p>
            </div>
          </div>
          <div className="row justify-content-center">
            {productsData.map((element) => (
              <div className="col-md-4 col-lg-2 hover-zoom text-sm-wrap col-sm-3 col-10 text-center bg-white m-2  p-2 align-content-start">
                <figure className="figure ">
                  <img src={element.image} alt="" className="figure-img img-fluid  image" />
                  <figure-caption>
                    <h1>{element.title}</h1>
                    <div>
                      {Array(Math.ceil(element.rating.rate))
                        .fill(0)
                        .map((_, index) => index + 1)
                        .map((element) => (
                          <span class="fa fa-star checked"></span>
                        ))}
                      {Array(5 - Math.ceil(element.rating.rate))
                        .fill(0)
                        .map((_, index) => index + 1)
                        .map((element) => (
                          <span class="fa fa-star unchecked"></span>
                        ))}
                      <span style={{ padding: "5px" }}>
                        {element.rating.count} ratings
                      </span>
                    </div>
                    <h2>
                      <span style={({ padding: "5px" }, { fontSize: "1.2em" })}>
                        $
                      </span>
                      {element.price}
                    </h2>
                  </figure-caption>
                </figure>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}
